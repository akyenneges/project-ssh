#!/bin/bash
#Création d'un utilisateur

echo "Entrez un nom d'utilisateurs : \n"
read user
useradd -m -p "$PASSWORD" "$USER"
usermod -aG sudo "$USER"

printf "$USER" "a obtenu les droit sudo" \n

# Copier le fichier de configuration sur le serveur

cp ./config__files/ssh/sshd_config /etc/ssh/sshd_config

cp ./config__files/ssh/Banner /etc/Banner




#Insertion de la clé public

printf "Ajoutez la clé public :"  \n
read key

echo $key >> gitc.txt

cat gitc.txt >> ~/ .ssh/authorized_keys

# Redemarrer le service sshd 

systemctl restart sshd 

rm gitc.txt

